const Colors = {
    primary:'rgb(224,74,66)',
    baseRed:'rgb(224,74,66)',
    baseRedLight:'rgba(224,74,66,0.6)',
    SplashButtonRed:'rgb(224,74,66)',
    SplashButtonWhite:'rgb(230,231,232)',
    OrTextColor:"#ccc",
    OrLineColor:"#ccc",
    TitleAuthRed:'rgb(224,74,66)',
    ColorLightText:"#898989",
    baseWhite:"#fff",
    mainDark:"#898989",
    mainDarkText:"#000",
    fbButtonColor:"#3b5998",
    borderColor: "#ccc",
    transparent:"transparent",
    tabBg: "#eee",
    tagYellowBg: "rgb(228,174,52)",
    orange :'#e24e42',
    yellow:'#e6af27',
    greyDark:'#141414',
    grey:'#bcbcbc',
    heading:'#1c2023',
    content:'#6a6a6a',
    baseBlack:'#000',
    onlineColor:"#2ecc71"
  }
  
  export default Colors
  