const Constant = {
    baseurl:'https://staging-services.pointrecognition.com/v1/',
    text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.',
    blackColor: "black",
    grey: '#929292',
    white: 'white',
    grey_light: '#f7f8f9',
    // arrowDown: require("../assets/downArrow.png"),
}
export default Constant;