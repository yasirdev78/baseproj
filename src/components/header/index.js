import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import { Touchable } from '../';
import { Constants } from '../../utils';

const Header = ({
    title='',
    showLeft=false,
    leftIcon=Constants.backIcon,
    rightIconOne=Constants.backIcon,
    rightIconSec=Constants.backIcon,
    onLeftPress,
    onRightPress
}) => (
    <View style={styles.view}>
        <View style={styles.statusBar} />
        <View style={styles.buttonView}>
            <Touchable onPress={onLeftPress}>
                <Image source={leftIcon} style={styles.backIcon} />
            </Touchable>
        </View>
        <View style={styles.titleContainer}>
            <Text style={styles.title}>{title}</Text>
        </View>
        <View style={styles.buttonView}>
            <Touchable onPress={onRightPress}>
                <Image source={rightIconOne} style={styles.backIcon} />
            </Touchable>
        </View>
    </View>
);

export {Header};
