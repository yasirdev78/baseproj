import React from 'react';
import {TouchableOpacity } from 'react-native';

const Touchable = ({
    children,
    onPress
}) => (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
        {children}
    </TouchableOpacity>
);

export {Touchable};
