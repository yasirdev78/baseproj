import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { Colors } from '../../utils';

class Input extends Component {
  
  render() {
      const{
        onSubmitEditing,
        blurOnSubmit,
        value='',
        onChangeText,
        placeholder='',
        placeholderTextColor=Colors.placeholderColor,
      } = this.props
    return (
      <TextInput
        onSubmitEditing={onSubmitEditing}
        blurOnSubmit={blurOnSubmit}
        value={value}
        onChangeText={onChangeText}
        autoCapitalize='none'
        autoCorrect={false}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        style={styles.input}
      />
    );
  }
}
export {Input}
