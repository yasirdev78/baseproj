const styles = {
    spinnerStyle: {
      flex: 1,
      width:'100%',
      height:'100%',
      justifyContent: 'center',
      alignItems: 'center',
      position:"absolute",
      zIndex:99,
      backgroundColor:"rgba(255,255,255,0.8)",
      left:0,
      top:0,
    }
};
export default styles;
  