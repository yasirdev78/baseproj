import React from 'react';
import { View, ActivityIndicator,Dimensions } from 'react-native';
const Spinner = ({ size }) => {
  return (
    <View style={styles.spinnerStyle}>
      <ActivityIndicator size={size || 'large'} />
    </View>
  );
};

export default Spinner;